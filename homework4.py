my_list = [1, 567, 56, 908, 7, 15, 105, 201, 306]

for number in my_list:
	if number > 100:print(number)

##############################

my_list = [1, 567, 56, 908, 7, 15, 105, 201, 306]
my_result = []
for number in my_list:
	if number > 100:my_result.append(number)
print(my_result)

##############################

my_list = [1, 2, 45, 23]

if len(my_list) < 2: my_list.append(0)
else: my_list.append(my_list[-1] + my_list[-2])

print(my_list)

##############################
my_list_1 = " 123456789"
my_list_2 = "0123456789"
my_result = []
for symb_1 in my_list_1:
	for symb_2 in my_list_2:
		my_result.append(int(symb_1 + symb_2))
print(my_result)